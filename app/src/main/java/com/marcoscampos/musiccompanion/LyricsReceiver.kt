package com.marcoscampos.musiccompanion

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.AsyncTask.execute
import android.util.Log


/**
 * Created by marcos on 6/7/18.
 */
class LyricsReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {

        intent?.let {
            val action = intent.action
            val cmd = intent.getStringExtra("command")
            val artist = intent.getStringExtra("artist")
            val music = intent.getStringExtra("track")
            val isPlaying = intent.getBooleanExtra("playing",false)

            if (isPlaying) {
                Log.v("teste","$artist / $music")
            } else {
                Log.v("teste","nada tocando")
            }
        }
    }
}