package com.marcoscampos.musiccompanion

import android.content.BroadcastReceiver
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import android.widget.Toast
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class MainActivity : AppCompatActivity() {

    val retrofit = Retrofit.Builder()
            .baseUrl("https://api.lyrics.ovh/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.scroll_main)

        val title = findViewById<TextView>(R.id.text_title_lyric)
        val lyric = findViewById<TextView>(R.id.text_lyric)
        val about = findViewById<TextView>(R.id.text_about)
        val editProblem = findViewById<TextView>(R.id.text_edit_problem)

        val call = service().getLyrics("blink182","always")

        call.enqueue(object : Callback<LyricsData> {

            override fun onFailure(call: Call<LyricsData>?, t: Throwable?) {
                Log.e("onFailure error", t?.message)
            }

            override fun onResponse(call: Call<LyricsData>?, response: Response<LyricsData>?) {
                response?.body()?.let {
                    val lyricResponse : LyricsData = it
                    lyric.text = lyricResponse.lyrics
                }
            }
        })

        registerIntentService()

    }

    fun registerIntentService() {
        val intenctFilter = IntentFilter()
        intenctFilter.addAction("com.android.music.musicservicecommand");
        intenctFilter.addAction("com.android.music.metachanged");
        intenctFilter.addAction("com.android.music.playstatechanged");
        intenctFilter.addAction("com.android.music.updateprogress");
        registerReceiver(getInstanceReceiver(), intenctFilter)
    }

    fun getInstanceReceiver() : BroadcastReceiver {
        return LyricsReceiver()
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(getInstanceReceiver())
    }

    fun service() : LyricsService = retrofit.create(LyricsService::class.java)
}
