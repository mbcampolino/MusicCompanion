package com.marcoscampos.musiccompanion

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface LyricsService {

    @GET("{artist}/{music}")
    fun getLyrics(@Path("artist") artist: String, @Path("music") music: String) : Call<LyricsData>

}